# vca-qe



## Prerequisite

QE-6.8

## Code and executable

The code to mix two pseudopotentials is "virtual_v3.f90" in folder "upflib" under the parent directory of QE-6.8.  The code should be compiled along with QE and this creates an executable "virtual_v3.x".

## Example

Assume that you are mixing two pseudopotentials W.upf and Re.upf in proportion to create A.upf, i.e., A = x W + (1-x) Re,
where x is known as the mixing parameter. The command line arguments are as follows:

Usage: virtual_v3.x <argument 1> <argument 2> <argument 3> <argument 4> <argument 5>

argument 1: Name (path) of the first pseudopotential file. ex: W.upf
argument 2: Name (path) of the second pseudopotential file. ex: Re.upf
argument 3: Mixing parameter x (0 < x < 1). ex: 0.1
argument 4: A species name for the virtual atom. Ex: A (this is more for notation, not actually any symbol for the species)
argument 5: Name (path) of the resultant mixed pseudopotential file. Ex: A.upf


Finally, the complete command looks as follows:

virtual_v3.x W.upf Re.upf 0.1 A A.upf

The generated mixed pseudopotential can be used just like any regular pseudopotential in QE.
